<?php

namespace App\Repositories;

use App\Models\Historical;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HistoricalRepository
 * @package App\Repositories
 * @version January 14, 2018, 5:20 pm UTC
 *
 * @method Historical findWithoutFail($id, $columns = ['*'])
 * @method Historical find($id, $columns = ['*'])
 * @method Historical first($columns = ['*'])
*/
class HistoricalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'comments',
        'vehicle_id',
        'start_time',
        'end_time',
        'total_value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Historical::class;
    }
}
