<?php

namespace App\Repositories;

use App\Models\Rate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RateRepository
 * @package App\Repositories
 * @version January 14, 2018, 5:15 pm UTC
 *
 * @method Rate findWithoutFail($id, $columns = ['*'])
 * @method Rate find($id, $columns = ['*'])
 * @method Rate first($columns = ['*'])
*/
class RateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'vehicle_type',
        'minute_value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rate::class;
    }
}
