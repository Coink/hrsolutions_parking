<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Historical
 * @package App\Models
 * @version January 14, 2018, 5:20 pm UTC
 *
 * @property string.250 comments
 * @property unsignedInteger vehicle_id
 * @property timestamps start_time
 * @property timestamps end_time
 * @property double total_value
 */
class Historical extends Model
{
    use SoftDeletes;

    public $table = 'historicals';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        "comments",
        "start_time",
        "total_value",
        "vehicle_id",
        "end_time"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vehicle_id' => 'required'
    ];


    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

}
