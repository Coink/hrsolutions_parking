<?php

use App\Models\Historical;
use App\Repositories\HistoricalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HistoricalRepositoryTest extends TestCase
{
    use MakeHistoricalTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var HistoricalRepository
     */
    protected $historicalRepo;

    public function setUp()
    {
        parent::setUp();
        $this->historicalRepo = App::make(HistoricalRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateHistorical()
    {
        $historical = $this->fakeHistoricalData();
        $createdHistorical = $this->historicalRepo->create($historical);
        $createdHistorical = $createdHistorical->toArray();
        $this->assertArrayHasKey('id', $createdHistorical);
        $this->assertNotNull($createdHistorical['id'], 'Created Historical must have id specified');
        $this->assertNotNull(Historical::find($createdHistorical['id']), 'Historical with given id must be in DB');
        $this->assertModelData($historical, $createdHistorical);
    }

    /**
     * @test read
     */
    public function testReadHistorical()
    {
        $historical = $this->makeHistorical();
        $dbHistorical = $this->historicalRepo->find($historical->id);
        $dbHistorical = $dbHistorical->toArray();
        $this->assertModelData($historical->toArray(), $dbHistorical);
    }

    /**
     * @test update
     */
    public function testUpdateHistorical()
    {
        $historical = $this->makeHistorical();
        $fakeHistorical = $this->fakeHistoricalData();
        $updatedHistorical = $this->historicalRepo->update($fakeHistorical, $historical->id);
        $this->assertModelData($fakeHistorical, $updatedHistorical->toArray());
        $dbHistorical = $this->historicalRepo->find($historical->id);
        $this->assertModelData($fakeHistorical, $dbHistorical->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteHistorical()
    {
        $historical = $this->makeHistorical();
        $resp = $this->historicalRepo->delete($historical->id);
        $this->assertTrue($resp);
        $this->assertNull(Historical::find($historical->id), 'Historical should not exist in DB');
    }
}
